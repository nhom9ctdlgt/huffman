#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define MAXNODE 511

#include <iostream>
#include <cstdint>
#include <string>
#include <Windows.h>

using namespace std;

/* File compression structure
	+------------------------+	0
	|	header				 |
	| + 12 bytes sign		 |
	| + 5120 bytes tBit		 |
	| + 4 bytes num of files |
	+------------------------+	5136
	|	list file info		 |
	| + file 1				 |
	| + file 2				 |
	| ....					 |
	+------------------------+	5136 + n x 280
	|	data				 |
	| + file 1				 |
	| + file 2				 |
	| ...					 |
	+------------------------+
*/

// dinh nghia cac cau truc
typedef struct
{
	unsigned char c;
	long nFreq;
	int nLeft;
	int nRight;
} HUFFNode;
typedef struct
{
	unsigned char c;
	int nLeft;
	int nRight;
} HUFFNode2;
typedef struct
{
	char fname[255];	// ten file
	char NullBit;		// so luong bit du
	uint64_t fsize;		// kt cu cua file
	uint64_t ps;		// vi tri bat dau
	uint64_t pe;		// vi tri ket thuc	
}FileInfo;



// class nen file
class Compression
{
public:
	// header           =======> 0
	char bitsign[12];
	char tBit[256][20];
	uint32_t filenum;
	// list file info   ========> 5136
	FileInfo *list;

	// Huff Tree
	HUFFNode HuffTree[MAXNODE];	// cay huff tree
	

	// Methos
	bool Input(char *s, int i);
	bool Output(char *s, char * si, int i);
	void MakeHuffTree();
	void MakeBit(char *a, int i, int root);
	void GetFileName(char *s);

	Compression();
	~Compression();
	void Compress(char *si, char *so);
	void Renew();
};

// class giai nen

class DeCompression
{
public:

	HUFFNode2 HuffTree[MAXNODE];	// cay huff tree
	char tBit[256][20];
	uint32_t filenum;
	FileInfo *list;
	FILE *f;

	bool Input(char *s);
	bool Output(int i, char *si);
	void MakeHuffTree();

	DeCompression();
	~DeCompression();
	void DeCompress(char *si);
};