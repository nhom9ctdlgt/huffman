#include "Huffman.h"
#include <stdio.h>
#include <time.h>
void main()
{
	clock_t b = clock();
	Compression cs;
	cs.Compress("in.jpg", "out.huff");

	DeCompression ds;
	ds.DeCompress("out.huff", "out.jpg");
	clock_t e = clock();
	printf("%f", (float)(e - b) / CLOCKS_PER_SEC);
}