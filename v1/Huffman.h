#pragma once
#define MAX_NODES 511
#define _CRT_SECURE_NO_WARNINGS
typedef struct
{
	unsigned char c;
	long nFreq;
	int nLeft;
	int nRight;
} HUFFNode;

typedef struct
{
	unsigned char c;
	int nLeft;
	int nRight;
} HUFFNode2;

class Compression
{
private:

	HUFFNode HuffTree[MAX_NODES];	// cay huff tree
	char tBit[256][20];

	bool Input(char *s);
	bool Output(char *s, char *si);
	void MakeHuffTree();
	void MakeBit(char *a, int i, int root);

public:
	Compression();
	void Compress(char *si, char *so);
};


class DeCompression
{
private:

	HUFFNode2 HuffTree[MAX_NODES];	// cay huff tree
	char tBit[256][20];
	char it;

	bool Input(char *s);
	bool Output(char *s, char *si);
	void MakeHuffTree();

public:
	DeCompression();
	void DeCompress(char *si, char *so);
};