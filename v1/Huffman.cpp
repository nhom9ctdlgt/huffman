#include <stdio.h>
#include <string.h>
#include "Huffman.h"
#define MAXBUFF 8

// class nen
Compression::Compression()
{
	int i = 256;
	while (--i >= 0)
	{
		HuffTree[i].c = i;
		HuffTree[i].nFreq = 0;
		HuffTree[i].nLeft = HuffTree[i].nRight = -1;
		memset(tBit[i], 0, 20);
	}
}
bool Compression::Input(char *s)
{
	FILE *f;
	f = fopen(s, "rb");
	if (f == NULL)
		return false;
	
	long n, nt;
	unsigned char buff[MAXBUFF];
	// xac dinh kich thuoc tap tin
	fseek(f, 0L, SEEK_END);
	n = ftell(f);
	rewind(f);
	// doc file
	nt = n % MAXBUFF;
	n = n / MAXBUFF;
	for (int i = 0; i < n; i++)
	{
		fread(buff, 1, MAXBUFF, f);
		HuffTree[buff[0]].nFreq++;
		HuffTree[buff[1]].nFreq++;
		HuffTree[buff[2]].nFreq++;
		HuffTree[buff[3]].nFreq++;
		HuffTree[buff[4]].nFreq++;
		HuffTree[buff[5]].nFreq++;
		HuffTree[buff[6]].nFreq++;
		HuffTree[buff[7]].nFreq++;
	}
	fread(buff, 1, nt, f);
	while (--nt >= 0)
		HuffTree[buff[nt]].nFreq++;
	fclose(f);
	return true;
}
bool Compression::Output(char *s, char *si)
{
	FILE *f, *fi;
	f = fopen(s, "wb");
	fi = fopen(si, "rb");
	if (f == NULL || fi == NULL)
		return false;
	char sig[2] = { 'V', 'B' };
	fwrite(sig, 1, 2, f);
	fwrite(tBit, 1, 5120, f);

	// ghi file
	unsigned char buffin = 0, buffout = 0;
	int i = 0;
	while (fread(&buffin, 1, 1, fi) > 0)
	{
		// new
		for (int j = 0; j < strlen(tBit[buffin]); j++)
		{
			if (i < 8)
			{
				buffout <<= 1;
				buffout |= tBit[buffin][j] - 48;
				i++;
			}
			if (i == 8)
			{
				i = 0;
				fwrite(&buffout, 1, 1, f);
			}
		}
	}
	
	if (i > 0)
	{
		buffout <<= (8 - i);
		fwrite(&buffout, 1, 1, f);
		buffout = 8 - i;
	}
	else
		buffout = 0;
	fwrite(&buffout, 1, 1, f);
	fclose(f);
	fclose(fi);
	return true;
}
void Compression::MakeHuffTree()
{
	HUFFNode tmp1;
	int n = 0;
	for (int i = 0; i < 256; i++)
	if (HuffTree[i].nFreq >0)
	{
		if (i > n)
		{
			while (HuffTree[n].nFreq > 0) n++;
			tmp1 = HuffTree[i];
			HuffTree[i] = HuffTree[n];
			HuffTree[n] = tmp1;
			n++;
		}
		else if (i == n)
		{
			n++;
		}
	}
	int id = 0;
	char mask[511];
	while (id < n - 1)
	{
		int im1=-1, im2=-1;
		for (int i = 0; i < n; i++)
		{
			if (mask[i] == 1)
				continue;
			if (im2 == -1 || HuffTree[im2].nFreq > HuffTree[i].nFreq)
				im2 = i;
			else if (HuffTree[im2].nFreq == HuffTree[i].nFreq && HuffTree[im2].c > HuffTree[i].c)
				im2 = i;
		}
		mask[im2] = 1;
		for (int i = 0; i < n; i++)
		{
			if (mask[i] == 1)
				continue;
			if (im1 == -1 || HuffTree[im1].nFreq > HuffTree[i].nFreq)
				im1 = i;
			else if (HuffTree[im2].nFreq == HuffTree[i].nFreq && HuffTree[im2].c > HuffTree[i].c)
				im2 = i;
		}
		mask[im1] = 1;

		HuffTree[n].nFreq = HuffTree[im1].nFreq + HuffTree[im2].nFreq;
		HuffTree[n].c = HuffTree[im2].c;
		HuffTree[n].nLeft = im2;
		HuffTree[n].nRight = im1;
		n++;
		id += 2;
	}
	// n-1 : dinh cay
	char a[20] = { 0 };
	MakeBit(a, 0, n - 1);
}
void Compression::MakeBit(char *a, int i, int root)
{
	if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
	{
		char saved = a[i];
		a[i] = 0;
		strcpy(tBit[HuffTree[root].c], a);
		a[i] = saved;
		return;
	}		
	a[i] = 48;
	MakeBit(a, i + 1, HuffTree[root].nLeft);
	a[i] = 49;
	MakeBit(a, i + 1, HuffTree[root].nRight);
	a[i] = 0;
}
void Compression::Compress(char *si, char *so)
{
	Input(si);
	MakeHuffTree();
	Output(so,si);
}


// class  giai nen
DeCompression::DeCompression()
{
	int i = 512;
	while (--i >= 0)
		HuffTree[i].nLeft = HuffTree[i].nRight = -1;
}
bool DeCompression::Input(char *s)
{
	FILE *f;
	f = fopen(s, "rb");
	if (f == NULL)
		return false;
	fseek(f, 2, SEEK_SET);
	fread(tBit, 1, 5120, f);
	fseek(f, -1, SEEK_END);
	fread(&it, 1, 1, f);
	fclose(f);
	return true;
}
bool DeCompression::Output(char *s, char *si)
{
	FILE *f, *fi;
	f = fopen(s, "wb");
	fi = fopen(si, "rb");
	if (f == NULL || fi == NULL)
		return false;

	long n,pcur;
	fseek(fi, 0L, SEEK_END);
	n = ftell(fi);	// kich thuoc tt
	n = n - 1;		// loai bo byte chua so bit thua o cuoi
	pcur = 5122;	// vi tri bat dau dac du lieu
	fseek(fi, pcur, SEEK_SET);

	int root = 0, id = 0; // root: vi tri node dau tien trong cay, id: bit dang xu ly trong buff
	unsigned char buff; // bo dem
	while (pcur < n)
	{
		fread(&buff, 1, 1, fi);
		pcur++;
		if (pcur == n)
			id = it;
		while (id < 8)
		{
			if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
			{
				fwrite(&HuffTree[root].c, 1, 1, f);
				root = 0;
			}
			else
			{
				unsigned char x;
				x = buff & 128;
				x >>= 7;
				buff <<= 1;
				if (x == 1)
					root = HuffTree[root].nRight;
				else
					root = HuffTree[root].nLeft;
				id++;
			}
		}
		if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
		{
			fwrite(&HuffTree[root].c, 1, 1, f);
			root = 0;
		}
		id = 0;
	}


	// dong file
	fclose(f);
	fclose(fi);
	return true;
}
void DeCompression::MakeHuffTree()
{
	int inext = 1;
	int id;
	for (int i = 0; i < 256; i++)
	if (tBit[i][0] != 0)
	{
		id = 0;
		int n = strlen(tBit[i]), j;
		for (j = 0; j < n - 1; j++)
		{
			if (tBit[i][j] == '1')
			{
				if (HuffTree[id].nRight == -1)
				{
					HuffTree[id].nRight = inext;
					inext++;
				}
				id = HuffTree[id].nRight;
			}
			else
			{
				if (HuffTree[id].nLeft == -1)
				{
					HuffTree[id].nLeft = inext;
					inext++;
				}
				id = HuffTree[id].nLeft;
			}
		}
		// nut la:
		if (tBit[i][j] == '1')
			HuffTree[id].nRight = inext;
		else
			HuffTree[id].nLeft = inext;
		HuffTree[inext].c = i;
		inext++;
	}
}
void DeCompression::DeCompress(char *si, char *so)
{
	Input(si);
	MakeHuffTree();
	Output(so, si);
}