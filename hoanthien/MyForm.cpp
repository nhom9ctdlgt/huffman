﻿#include "MyForm.h"

using namespace System;
using namespace System::Windows::Forms;

[STAThread]
int main(array<System::String ^> ^args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	Application::Run(gcnew MyForm());
	return 0;
}

Void MyForm::bLoadFolder_Click(System::Object^  sender, System::EventArgs^  e)
{
	de->lammoi();
	cs->lammoi();
	listView1->Items->Clear();
	listView1->CheckBoxes = false;
	::DialogResult res = dgfolder->ShowDialog();
	if (res == ::DialogResult::OK)
	{
		String ^t = dgfolder->SelectedPath;
		sprintf(si, "%s", t);
		cs->laythongtincacfile(si);
		if (cs->sotaptin == 0)
			return;
		dir->Text = t;
		bCompress->Enabled = true;
		bDeCompress1->Enabled = false;
		bDeCompress2->Enabled = false;
		for (int i = 0; i < cs->sotaptin; i++)
		{
			t = gcnew String(cs->danhsach[i].fname);
			listView1->Items->Add(t);
		}
	}
}
Void MyForm::bCompress_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (cs->sotaptin == 0)
		return;
	::DialogResult res = dgfsave->ShowDialog();
	if (res == ::DialogResult::OK)
	{
		String ^t = dgfsave->FileName;
		sprintf(so, "%s", t);
		listView1->Items->Clear();
		cs->xuatfilechinh(si, so);
		char buff[100];
		for (int i = 0; i < cs->sotaptin; i++)
		{
			ListViewItem t;
			array<String^>^ subItems = gcnew array<String^>(3);
			subItems[0] = gcnew String(cs->danhsach[i].fname);
			sprintf(buff, "%ld", cs->danhsach[i].fsize);
			subItems[1] = gcnew String(buff);
			sprintf(buff, "%ld", cs->danhsach[i].pe - cs->danhsach[i].ps);
			subItems[2] = gcnew String(buff);
			ListViewItem^ itm = gcnew ListViewItem(subItems);
			// Add the ListViewItem to the ListView
			listView1->Items->Add(itm);
		}
		MessageBox::Show("Thành công", "Trạng thái");
		cs->lammoi();
		memset(si, 0, 256);
		memset(so, 0, 256);
		bCompress->Enabled = false;
	}
}
Void MyForm::bLoadFile_Click(System::Object^  sender, System::EventArgs^  e)
{
	cs->lammoi();
	de->lammoi();
	listView1->Items->Clear();
	listView1->CheckBoxes = true;
	::DialogResult res = dgfopen->ShowDialog();
	if (res == ::DialogResult::OK)
	{
		String ^t = dgfopen->FileName;
		sprintf(si, "%s", t);
		de->docfile(si);
		if (de->sotaptin == 0)
			return;
		dir->Text = t;
		bCompress->Enabled = false;
		bDeCompress1->Enabled = true;
		bDeCompress2->Enabled = true;
		radioButton1->Enabled = true;
		radioButton2->Enabled = true;
		radioButton2->Checked = true;

		char buff[100];
		for (int i = 0; i < de->sotaptin; i++)
		{
			ListViewItem t;
			array<String^>^ subItems = gcnew array<String^>(3);
			subItems[0] = gcnew String(de->danhsach[i].fname);
			sprintf(buff, "%ld", de->danhsach[i].fsize);
			subItems[1] = gcnew String(buff);
			sprintf(buff, "%ld", de->danhsach[i].pe - de->danhsach[i].ps);
			subItems[2] = gcnew String(buff);
			ListViewItem^ itm = gcnew ListViewItem(subItems);
			// Add the ListViewItem to the ListView
			listView1->Items->Add(itm);
		}
	}
}
Void MyForm::bDeCompress1_Click(System::Object^  sender, System::EventArgs^  e)
{
	if (listView1->CheckedItems->Count == 0)
		return;
	::DialogResult res = dgfolder->ShowDialog();
	if (res == ::DialogResult::OK)
	{
		String ^t = dgfolder->SelectedPath;
		sprintf(so, "%s", t);
		de->taocayhuff();
		
		for (int i = 0; i < listView1->CheckedItems->Count; i++)
		{
			int j = listView1->Items->IndexOf(listView1->CheckedItems[i]);
			de->xuatfile(so, j);
		}
		MessageBox::Show("Thành công", "Trạng thái");
		de->lammoi();
		memset(si, 0, 256);
		memset(so, 0, 256);
		bDeCompress1->Enabled = false;
		bDeCompress2->Enabled = false;
		radioButton1->Enabled = false;
		radioButton2->Enabled = false;
	}
}
Void MyForm::bDeCompress2_Click(System::Object^  sender, System::EventArgs^  e)
{
	::DialogResult res = dgfolder->ShowDialog();
	if (res == ::DialogResult::OK)
	{
		String ^t = dgfolder->SelectedPath;
		sprintf(so, "%s", t);
		de->taocayhuff();
		for (int i = 0; i < de->sotaptin; i++)
		{
			de->xuatfile(so, i);
		}
		MessageBox::Show("Thành công", "Trạng thái");
		de->lammoi();
		memset(si, 0, 256);
		memset(so, 0, 256);
		bDeCompress1->Enabled = false;
		bDeCompress2->Enabled = false;
		radioButton1->Enabled = false;
		radioButton2->Enabled = false;
	}
}
Void MyForm::radioButton1_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	if (radioButton1->Checked)
	{
		for (int i = 0; i < listView1->Items->Count; i++)
			listView1->Items[i]->Checked = true;
	}
}
Void MyForm::radioButton2_CheckedChanged(System::Object^  sender, System::EventArgs^  e)
{
	if (radioButton2->Checked)
	{
		for (int i = 0; i < listView1->Items->Count; i++)
			listView1->Items[i]->Checked = false;
	}
}
Void MyForm::MyForm_Load(System::Object^  sender, System::EventArgs^  e)
{
	cs = new NenFile;
	de = new GiaiNenFile;
	si = new char[256];
	so = new char[256];

	cs->sotaptin = 0;
	cs->lammoi();
	de->sotaptin = 0;
	de->f = NULL;
	de->lammoi();
}
Void MyForm::MyForm_Closed(System::Object^  sender, System::EventArgs^  e)
{
	cs->lammoi();
	de->lammoi();
}