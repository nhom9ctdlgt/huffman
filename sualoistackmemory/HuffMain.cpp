#define MAXBUFF 8
#include "HuffMain.h"

void NenFile::lammoi()
{
	if (sotaptin > 0)
		delete[]danhsach;
	danhsach = NULL;
	sotaptin = 0;
	for (int i = 0; i < 256; i++)
		tanso[i] = 0;
	for (int i = 0; i < 512; i++)
	{
		HuffTree[i].nFreq = 0;
		HuffTree[i].nLeft = HuffTree[i].nRight = -1;
	}
}
void NenFile::laythongtincacfile(char *s)
{
	char thumuc[256];
	sprintf(thumuc, "%s\\*.*", s);
	WIN32_FIND_DATAA fileData;
	HANDLE hFind;
	if (!((hFind = FindFirstFileA(thumuc, &fileData)) == INVALID_HANDLE_VALUE))
	{
		do
		{
			if (!(fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				sotaptin++;
		} while (FindNextFileA(hFind, &fileData));
	}
	FindClose(hFind);
	// list num file
	if (sotaptin == 0)
		return;
	danhsach = new FileInfo[sotaptin];
	int i = 0;
	if (!((hFind = FindFirstFileA(thumuc, &fileData)) == INVALID_HANDLE_VALUE))
	{
		do
		{
			if (!(fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				strcpy(danhsach[i].fname, fileData.cFileName);
				i++;
			}
		} while (FindNextFileA(hFind, &fileData));
	}
	FindClose(hFind);
}
void NenFile::docfile(char *taptin, int i)
{
	FILE *f;
	f = fopen(taptin, "rb");

	long n, nt;
	unsigned char buff[MAXBUFF];
	// xac dinh kich thuoc tap tin
	fseek(f, 0L, SEEK_END);
	n = ftell(f);
	danhsach[i].fsize = n;
	rewind(f);
	// doc file
	nt = n % MAXBUFF;
	n = n / MAXBUFF;
	for (int i = 0; i < n; i++)
	{
		fread(buff, 1, MAXBUFF, f);
		tanso[buff[0]]++;
		tanso[buff[1]]++;
		tanso[buff[2]]++;
		tanso[buff[3]]++;
		tanso[buff[4]]++;
		tanso[buff[5]]++;
		tanso[buff[6]]++;
		tanso[buff[7]]++;
	}
	fread(buff, 1, nt, f);
	while (--nt >= 0)
		tanso[buff[nt]]++;
	fclose(f);
}
void NenFile::taocayhuff()
{
	HUFFNode tmp1;
	int n = 0;
	for (int i = 0; i < 256; i++)
	if (tanso[i] >0)
	{
		HuffTree[n].c = i;
		HuffTree[n].nFreq = tanso[i];
		n++;
	}
	int id = 0;
	char mask[512] = { 0 };
	while (id < n - 1)
	{
		int im1 = -1, im2 = -1;
		for (int i = 0; i < n; i++)
		{
			if (mask[i] == 1)
				continue;
			if (im2 == -1 || HuffTree[im2].nFreq > HuffTree[i].nFreq)
				im2 = i;
			else if (HuffTree[im2].nFreq == HuffTree[i].nFreq && HuffTree[im2].c > HuffTree[i].c)
				im2 = i;
		}
		mask[im2] = 1;
		for (int i = 0; i < n; i++)
		{
			if (mask[i] == 1)
				continue;
			if (im1 == -1 || HuffTree[im1].nFreq > HuffTree[i].nFreq)
				im1 = i;
			else if (HuffTree[im2].nFreq == HuffTree[i].nFreq && HuffTree[im2].c > HuffTree[i].c)
				im1 = i;
		}
		mask[im1] = 1;

		HuffTree[n].nFreq = HuffTree[im1].nFreq + HuffTree[im2].nFreq;
		HuffTree[n].c = HuffTree[im2].c;
		HuffTree[n].nLeft = im2;
		HuffTree[n].nRight = im1;
		n++;
		id += 2;
	}
	// n-1 : dinh cay
	char a[20] = { 0 };
	taomabit(a, 0, n - 1);
}
void NenFile::taomabit(char *a, int i, int root)
{
	if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
	{
		char saved = a[i];
		a[i] = 0;
		strcpy(mabit[HuffTree[root].c], a);
		a[i] = saved;
		return;
	}
	a[i] = 48;
	taomabit(a, i + 1, HuffTree[root].nLeft);
	a[i] = 49;
	taomabit(a, i + 1, HuffTree[root].nRight);
	a[i] = 0;
}
void NenFile::xuatfiletam(char *thumuc, int id)
{
	FILE *f, *fi;
	char s[256], si[256];
	sprintf(s, "%s\\%s.tmp", thumuc, danhsach[id].fname);
	sprintf(si, "%s\\%s", thumuc, danhsach[id].fname);

	f = fopen(s, "wb");
	fi = fopen(si, "rb");

	// ghi file
	unsigned char buffin = 0, buffout = 0;
	int i = 0;
	while (fread(&buffin, 1, 1, fi) > 0)
	{
		// new
		for (int j = 0; j < strlen(mabit[buffin]); j++)
		{
			if (i < 8)
			{
				buffout <<= 1;
				buffout |= mabit[buffin][j] - 48;
				i++;
			}
			if (i == 8)
			{
				i = 0;
				fwrite(&buffout, 1, 1, f);
			}
		}
	}

	if (i > 0)
	{
		buffout <<= (8 - i);
		fwrite(&buffout, 1, 1, f);
		buffout = 8 - i;
	}
	else
		buffout = 0;
	danhsach[id].NullBit = buffout;
	fclose(f);
	fclose(fi);
}
void NenFile::xuatfilechinh(char *thumuc, char *taptin)
{
	char t1[256];
	//doc file va tao bang tan so
	for (int i = 0; i < sotaptin; i++)
	{
		sprintf(t1, "%s\\%s", thumuc, danhsach[i].fname);
		docfile(t1, i);
	}
	taocayhuff();
	for (int i = 0; i < sotaptin; i++)
		xuatfiletam(thumuc, i);
	FILE *f, *fi;
	f = fopen(taptin, "wb");
	unsigned char buff[MAXBUFF];
	buff[0] = 'G';
	buff[1] = '9';
	fwrite(&buff, 1, 2, f);
	fwrite(tanso, sizeof(uint64_t), 256, f);
	fwrite(&sotaptin, sizeof(uint32_t), 1, f);
	char sto[256];
	int n, nt;

	for (int i = 0; i < sotaptin; i++)
	{
		sprintf(sto, "%s\\%s.tmp", thumuc, danhsach[i].fname);
		fi = fopen(sto, "rb");
		fseek(fi, 0L, SEEK_END);
		danhsach[i].pe = ftell(fi);
		fclose(fi);
		if (i == 0)
		{
			danhsach[i].ps = 2054 + sotaptin * sizeof(FileInfo);
			danhsach[i].pe += danhsach[i].ps;
		}
		else
		{
			danhsach[i].ps = danhsach[i - 1].pe;
			danhsach[i].pe += danhsach[i].ps;
		}
	}
	fwrite(danhsach, sizeof(FileInfo), sotaptin, f);
	for (int i = 0; i < sotaptin; i++)
	{
		sprintf(sto, "%s\\%s.tmp", thumuc, danhsach[i].fname);
		fi = fopen(sto, "rb");
		fseek(fi, 0L, SEEK_END);
		n = ftell(fi);
		rewind(fi);
		nt = n % MAXBUFF;
		n = n / MAXBUFF;
		for (int j = 0; j < n; j++)
		{
			fread(buff, 1, MAXBUFF, fi);
			fwrite(buff, 1, MAXBUFF, f);
		}
		fread(buff, 1, nt, fi);
		fwrite(buff, 1, nt, f);
		fclose(fi);
		remove(sto);
	}
	fclose(f);
}


void GiaiNenFile::lammoi()
{
	if (sotaptin > 0)
		delete[]danhsach;
	sotaptin = 0;
	for (int i = 0; i < 256; i++)
		tanso[i] = 0;
	for (int i = 0; i < 512; i++)
	{
		HuffTree[i].nFreq = 0;
		HuffTree[i].nLeft = HuffTree[i].nRight = -1;
	}
	nroot = 0;
	if (f != NULL)
		fclose(f);
	f = NULL;
}
void GiaiNenFile::docfile(char *taptin)
{
	f = fopen(taptin, "rb");
	fseek(f, 2, SEEK_SET);
	fread(tanso, sizeof(uint64_t), 256, f);
	fread(&sotaptin, sizeof(uint32_t), 1, f);
	danhsach = new FileInfo[sotaptin];
	fread(danhsach, sizeof(FileInfo), sotaptin, f);
}
void GiaiNenFile::taocayhuff()
{
	HUFFNode tmp1;
	int n = 0;
	for (int i = 0; i < 256; i++)
	if (tanso[i] >0)
	{
		HuffTree[n].c = i;
		HuffTree[n].nFreq = tanso[i];
		n++;
	}
	int id = 0;
	char mask[512] = { 0 };
	while (id < n - 1)
	{
		int im1 = -1, im2 = -1;
		for (int i = 0; i < n; i++)
		{
			if (mask[i] == 1)
				continue;
			if (im2 == -1 || HuffTree[im2].nFreq > HuffTree[i].nFreq)
				im2 = i;
			else if (HuffTree[im2].nFreq == HuffTree[i].nFreq && HuffTree[im2].c > HuffTree[i].c)
				im2 = i;
		}
		mask[im2] = 1;
		for (int i = 0; i < n; i++)
		{
			if (mask[i] == 1)
				continue;
			if (im1 == -1 || HuffTree[im1].nFreq > HuffTree[i].nFreq)
				im1 = i;
			else if (HuffTree[im2].nFreq == HuffTree[i].nFreq && HuffTree[im2].c > HuffTree[i].c)
				im1 = i;
		}
		mask[im1] = 1;

		HuffTree[n].nFreq = HuffTree[im1].nFreq + HuffTree[im2].nFreq;
		HuffTree[n].c = HuffTree[im2].c;
		HuffTree[n].nLeft = im2;
		HuffTree[n].nRight = im1;
		n++;
		id += 2;
	}
	nroot = n - 1;
}
bool GiaiNenFile::xuatfile(char *thumuc, int i)
{
	FILE *fo;
	char st[256];
	sprintf(st, "%s\\%s", thumuc, danhsach[i].fname);
	fo = fopen(st, "wb");
	fseek(f, danhsach[i].ps, SEEK_SET);
	long n, pcur;
	pcur = danhsach[i].ps;
	n = danhsach[i].pe;

	int root = nroot, id = 0; // root: vi tri node dau tien trong cay, id: bit dang xu ly trong buff
	unsigned char buff; // bo dem
	while (pcur < n)
	{
		fread(&buff, 1, 1, f);
		pcur++;
		if (pcur == n)
			id = danhsach[i].NullBit;
		while (id < 8)
		{
			if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
			{
				fwrite(&HuffTree[root].c, 1, 1, fo);
				root = nroot;
			}
			else
			{
				unsigned char x;
				x = buff & 128;
				x >>= 7;
				buff <<= 1;
				if (x == 1)
					root = HuffTree[root].nRight;
				else
					root = HuffTree[root].nLeft;
				id++;
			}
		}
		if (HuffTree[root].nLeft == -1 && HuffTree[root].nRight == -1)
		{
			fwrite(&HuffTree[root].c, 1, 1, fo);
			root = nroot;
		}
		id = 0;
	}
	// dong file
	fclose(fo);
	fo = fopen(st, "rb");
	fseek(fo, 0L, SEEK_END);
	n = ftell(fo);
	fclose(fo);
	return n == danhsach[i].fsize;
}